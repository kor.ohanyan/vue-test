import Vue from "vue";
import VueRouter from "vue-router";
import HomeView from "@/views/HomeView.vue";
import BagView from "@/views/BagView.vue";
import ProductView from "@/views/ProductView.vue";
import NotFoundView from "@/views/NotFoundView.vue";
import CategoryView from "@/views/CategoryView.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/product/:id",
    name: "product",
    props: true,
    component: ProductView,
  },
  {
    path: "/products/card",
    name: "card",
    props: true,
    component: BagView,
  },
  {
    path: "/category/:id",
    name: "category",
    props: true,
    component: CategoryView,
  },
  {
    path: "*",
    component: NotFoundView,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;

import http from "@/api/config.js";

export default {
  state: {
    categories: [],
    products: [],
    cartCount: [],
  },
  getters: {
    getCategories(state) {
      return state.categories;
    },
    getProducts(state) {
      return state.products;
    },
    getCartCount(state) {
      return state.cartCount;
    },
  },
  mutations: {
    updateCategories(state, categories) {
      state.categories = categories;
    },
    updateProducts(state, products) {
      state.products = products;
    },

    updateCartCount(state, id) {
      const letters = new Set(state.cartCount);
      if (letters.has(id)) {
        letters.delete(id);
      } else {
        letters.add(id);
      }

      state.cartCount = [...letters];
    },
  },
  actions: {
    async fetchCategories({ commit }) {
      try {
        const response = await http.get(`/categories`);
        commit("updateCategories", response.data.items);
      } catch (error) {
        console.error("Error fetching categories:", error);
      }
    },
    async fetchProducts({ commit }) {
      try {
        const response = await http.get(`/products`);
        commit("updateProducts", response.data.items);
      } catch (error) {
        console.error("Error fetching products:", error);
      }
    },
  },
};

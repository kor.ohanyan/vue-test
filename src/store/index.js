import Vue from "vue";
import Vuex from "vuex";
import shop from "./categories";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    shop,
  },
});
